#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk
include /usr/share/javahelper/java-vars.mk

export LEIN_HOME=$(CURDIR)/.lein
export LEIN_OFFLINE=true
NAME=trapperkeeper-webserver-jetty9

%:
	dh $@ --with javahelper --with maven_repo_helper

override_dh_auto_configure:
	cd debian && ln -sf /usr/share/maven-repo .

override_dh_auto_build:
	# "with-profile -dev" below is a workaround for
	# https://codeberg.org/leiningen/leiningen/issues/14
	# upstream needs to fix their project.clj
	lein with-profile -dev pom debian/pom.xml
	lein i18n make
	lein jar
	# symlinks so we don't need a version in debian/*.poms
	cd target && ln -sf $(NAME)-$(DEB_VERSION_UPSTREAM).jar $(NAME).jar
	cd target/test && ln -sf $(NAME)-$(DEB_VERSION_UPSTREAM)-test.jar $(NAME)-test.jar

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	lein test
endif

execute_before_dh_clean:
	rm -f .lein-failures debian/maven-repo debian/pom.xml resources/locales.clj
	rm -rf .lein resources/puppetlabs target
