From: Justin Stoller <justin.stoller@gmail.com>
Date: Fri, 24 May 2019 16:10:44 -0700
Subject: [PATCH] (maint) Disable EndpointIdentification

Previously, Jetty disabled Endpoint Identification by default as it is a best
practice for most webservers who often cannot identify clients
connecting to it. However, in 9.4.15 Jetty changed this default to
"HTTPS", which is the best practice for _client_ SslContexts. This
caused serious breakages throughout the Jetty ecosystem and since 9.4.16
Jetty introduced static inner classes of SslContextFactory, named Server
and Client, to create the correct contexts for each type of consumer.

Unfortunately, because we subclass SslContextFactory with our own
InternalSslContextFactory that overrides CRL handling, using these static
inner class factories is problematic. Consequently, this patch takes the
approach of simply setting the Endpoint Identification Algorithm to null
as was previously the default (and necessary in most server
environments).

This will cause a warning of overriding a deprecated method during
compilation in newer Java versions and our approach to handling CRLs
will need to be reworked should we use this codebase as a basis for a
trapperkeeper-webserver-jetty10 project.

For more info see linked issues to the implementing PR here:
https://github.com/eclipse/jetty.project/pull/3480/files#diff-58640db0f8f2cd84b7e653d1c1540913
---
 src/puppetlabs/trapperkeeper/services/webserver/jetty9_core.clj | 1 +
 1 file changed, 1 insertion(+)

Index: trapperkeeper-webserver-jetty9-clojure/src/puppetlabs/trapperkeeper/services/webserver/jetty9_core.clj
===================================================================
--- trapperkeeper-webserver-jetty9-clojure.orig/src/puppetlabs/trapperkeeper/services/webserver/jetty9_core.clj
+++ trapperkeeper-webserver-jetty9-clojure/src/puppetlabs/trapperkeeper/services/webserver/jetty9_core.clj
@@ -211,6 +211,7 @@
                   (.setKeyStore (:keystore keystore-config))
                   (.setKeyStorePassword (:key-password keystore-config))
                   (.setTrustStore (:truststore keystore-config))
+                  (.setEndpointIdentificationAlgorithm nil)
                   ;; Need to clear out the default cipher suite exclude list so
                   ;; that Jetty doesn't potentially remove one or more ciphers
                   ;; that we want to be included.
